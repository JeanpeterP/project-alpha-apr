# Generated by Django 4.0.6 on 2022-12-07 21:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("tasks", "0001_initial"),
    ]

    operations = [
        migrations.DeleteModel(
            name="Task",
        ),
    ]

from django.urls import path
from accounts.views import view_signup, view_login, view_logout


urlpatterns = [
    path("signup/", view_signup, name="signup"),
    path("login/", view_login, name="login"),
    path("logout/", view_logout, name="logout"),
]

from projects.models import Project
from tasks.models import Task
from django.contrib import admin


# Register your models here.
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "description",
        "id",
    ]


@admin.register(Task)
class Task(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "id",
    )
